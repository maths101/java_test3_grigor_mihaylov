package java_test3_grigor_mihaylov;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import java.util.*;

public class DicaGameApp extends Application{
	
	//set up the game object and the overall javafx
	public void start(Stage stage) {
		Group root = new Group(); 
		DiceGame game = new DiceGame();
		
		VBox overall = new VBox();
		HBox money = this.getMoneyBox(game);
		HBox m = this.getMessege(game);
		
		HBox bet = this.getActions(game);
		
		overall.getChildren().addAll(money,bet,m);
		root.getChildren().add(overall);
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BEIGE);	
		stage.setTitle("Dice Game"); 
		stage.setScene(scene); 
		stage.show(); 
		
	}
	//set up the mesag box and  pass the message textfeild  to the game
	private HBox getMessege(DiceGame game) {
		TextField message = new TextField();
		message.setEditable(false);
		message.setMinWidth(300);
	
		game.setMessage(message);
		
		HBox m = new HBox();
		m.getChildren().add(message);
		return m;
	}

	//set up the actions box and  pass the bet textfeild to the game
	//also adding the buttons
	private HBox getActions(DiceGame game) {
		Label betText = new Label("Place your bet: ");
		TextField betAmount = new TextField();
		game.setBet(betAmount);
		
		Button betOdd = game.getAction(0);
		Button betEven = game.getAction(1);
		
		HBox bet = new HBox();
		bet.getChildren().addAll(betText,betAmount,betOdd,betEven);
		return bet;
	}

	//set up the money box and  pass the money textfeild to the game
	private HBox getMoneyBox(DiceGame game) {
		Label moneyText = new Label("Money remaining $");
		TextField moneyAmount = new TextField("250.0");
		moneyAmount.setEditable(false);
		
		game.setMoney(moneyAmount);
		
		HBox money = new HBox();
		money.getChildren().addAll(moneyText,moneyAmount);
		
		return money;
	}
	//run the app
	 public static void main(String[] args) {
	       Application.launch(args);
	 }
	  
}
