package java_test3_grigor_mihaylov;

import java.util.Random;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class DiceGame {
	private TextField messgae;
	private TextField betAmount;
	private TextField moneyAmount;
	private static Random rn = new Random();
	//play the game, displays errors in case on invalid input, 
	//if user was better even remainder is 0, if odd - 1
	//also remove/add money thats lost/won
	public void playRound(int remainder) {
		double bet =0.0;
		try {
			bet = Double.parseDouble(betAmount.getText() );
		}catch(Exception e) {
			this.messgae.setText("Invalid bet. Please try again.");
			return;
		}
		double money = Double.parseDouble(moneyAmount.getText() );
		if(money <bet) {
			this.messgae.setText("Not enough money.");
			return;
		}
		
		int diceRoll = rn.nextInt(6);
		diceRoll++;
		if(diceRoll % 2 == remainder) {
			this.moneyAmount.setText(  String.valueOf(money+bet)    );
			this.betAmount.setText("");
			this.messgae.setText("The dice rolled " +diceRoll +"! You win $"+bet+"!");
		}else {
			this.moneyAmount.setText(  String.valueOf(money-bet)    );
			this.betAmount.setText("");
			this.messgae.setText("The dice rolled " +diceRoll +". You lose $"+bet+".");
		}
	}
	//get a botton fot the App for the game
	public Button getAction(int remainder) {
		Button b = new Button();
		String text = "";
		if(remainder == 1) {
			text = "BET ON ODD";
		}else {
			text="BET ON EVEN";
		}
		b.setText(text);
		b.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e)
			{
				playRound(remainder);
			}
		});
		return b;
	}
	//set the message field
	public void setMessage(TextField message){
		this.messgae =message;
	}
	//set the bet field
	public void setBet(TextField betAmount){
		this.betAmount=betAmount;
	}
	//set the money field
	public void setMoney(TextField moneyAmount){
		this.moneyAmount=moneyAmount;
	}
}
