package java_test3_grigor_mihaylov;

import java.util.Arrays;

public class Recursion {
		public static int recursiveCount(String[] words , int n ) {
			if(words.length == 0) {
				return 0;
			}
			int count = 0; 
			int index = words.length -1;
			System.out.println("WORD: "+words[index]);
			if( words[index].contains("w") && index >=n && index%2 ==1) {
				System.out.println("Counted");
				count++;
			}
			String[] nextWords = Arrays.copyOfRange(words, 0,index);
			
			return count + recursiveCount(nextWords,n);
		}
		
}
